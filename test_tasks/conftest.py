import json
import pytest
from selenium import webdriver


@pytest.fixture(scope='session')
def drv():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.maximize_window()
    driver.implicitly_wait(5)
    yield driver
    driver.quit()


@pytest.fixture
def get_user_data():
    """
    :return: Dict with user data
    """
    with open("/var/lib/jenkins/workspace/pl2/test_tasks/dt.json") as file:
        content = file.read()
        content_json = json.loads(content)
        yield content_json["new_user"]
    file.close()


@pytest.fixture
def get_user_edit():
    """
    :return: Dict with updated user data
    """
    with open("/var/lib/jenkins/workspace/pl2/test_tasks/dt.json") as file:
        content = file.read()
        content_json = json.loads(content)
        yield content_json["edit_user"]
    file.close()
