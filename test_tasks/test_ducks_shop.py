from tools.pages.main_page import MainPage
from tools.pages.edit_page import EditPage


def test_edit_name_ui(drv):
    """
    Test checks editing name in UI
    :param drv: Chrome driver
    """
    page = MainPage(drv)
    page.open()
    page.perform_login()
    edit = page.open_edit_page()
    result = edit.change_user_name()
    assert result.text == 'Changes saved successfully.'


def test_editing_from_db():
    """
    Test checks edited name on database
    :return: None
    """
    assert [('Renamed',)] == EditPage.get_name_from_database()


def test_ducks_price(drv):
    """
    :param drv: Chrome driver
    :return: None
    """
    page = MainPage(drv)
    page.open()
    duck = page.get_duck_page()
    cart = duck.add_items_to_cart()
    assert cart.get_final_price().text == '$54.00'


def test_removing_items(drv):
    """
    :param drv: Chrome driver
    :return: None
    """
    page = MainPage(drv)
    page.open()
    duck = page.get_duck_page()
    cart = duck.add_items_to_cart()
    assert cart.remove_items_from_cart().text == \
           "There are no items in your cart."
