from tools.pages.rest_api import RestApi


def test_add_new_user(get_user_data):
    """
    Test checks that new user was created
    :param get_user_data: Dict - user data
    """
    p = RestApi()
    assert p.add_new_user(get_user_data) == 200, \
        "User wasn't added"


def test_added_user(get_user_data):
    """
    Test checks that added user can get
    and checks firstName added user
    :param get_user_data: Dict - user data
    """
    p = RestApi()
    us_name = get_user_data["username"]
    data, code = p.get_user_name(us_name)
    assert get_user_data["firstName"] in data
    assert code == 200, "User wasn't added"


def test_change_name(get_user_data, get_user_edit):
    """
    Test checks that user data was changed
    :param get_user_data: Dict - user data
    :param get_user_edit: Dict - new user data
    """
    p = RestApi()
    us_name = get_user_data["username"]
    new_name = get_user_edit
    data, code = p.get_user_name(get_user_edit["username"])
    assert p.change_user_name(us_name, new_name) == 200
    assert get_user_edit['firstName'] in data
    assert code == 200, "User's data wasn't changed"


def test_removing_user(get_user_data):
    """
    Test check that test user was removed
    :param get_user_data: Dict - user data
    """
    p = RestApi()
    us_name = get_user_data["username"]
    del_code, get_code = p.remove_user(us_name)
    assert del_code == 200, "User wasn't removed"
    assert get_code == 404, "User wasn't removed"
