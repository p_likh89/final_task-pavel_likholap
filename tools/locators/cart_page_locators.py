from selenium.webdriver.common.by import By


class CartPageLocators:
    """
    Class for import
    locators from cart page
    """
    CART_AMOUNT = (By.XPATH, '//input[@type="number"]')
    CART_UPDATE = (By.XPATH, '//button[@name="update_cart_item"]')
    CART_FINALLY_PRICE = (By.CSS_SELECTOR,
                          "tr.footer > td:nth-child(2) > strong")
    CART_REMOVE_BTN = (By.XPATH, '//button[@name="remove_cart_item"]')
    CART_BLANK_TEXT = (By.CSS_SELECTOR, 'p > em')
