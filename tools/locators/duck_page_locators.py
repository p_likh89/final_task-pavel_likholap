from selenium.webdriver.common.by import By


class DuckPageLocators:
    """
    Class for import
    locators from duck page
    """
    DUCK_DD1 = (By.XPATH, '//select')
    DUCK_DD2 = (By.XPATH, '//option[@value="Small"]')
    DUCK_CART_BTN = (By.XPATH, '//button[@name="add_cart_product"]')
    DUCK_CART = (By.XPATH,
                 '(//a[@href = "http://192.168.99.1/litecart/en/checkout"])[3]')
