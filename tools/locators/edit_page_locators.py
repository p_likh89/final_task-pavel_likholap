from selenium.webdriver.common.by import By


class EditPageLocators:
    """
    Class for import
    locators from edit page
    """
    EDIT_NAME = (By.XPATH, "//input[@name='firstname']")
    EDIT_SAVE = (By.XPATH, "//button[@name='save']")
    EDIT_NOTICE = (By.CSS_SELECTOR, 'div.notice.success')
