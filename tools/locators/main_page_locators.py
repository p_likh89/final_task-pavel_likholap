from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Class for import
    locators from main page
    """
    MAIN_LOGIN = (By.CSS_SELECTOR, '#box-account-login > h3')
    MAIN_EMAIL = (By.XPATH, "//input[@name='email']")
    MAIN_PASSWORD = (By.XPATH, "//input[@name='password']")
    MAIN_LOGIN_BTN = (By.XPATH, "//button[@name='login']")
    MAIN_ACCOUNT = (By.CSS_SELECTOR, '#box-account > h3')
    MAIN_EDIT = (By.XPATH,
                 '(//a[@href="http://192.168.99.1/litecart/en/edit_account"])[1]')
    MAIN_DUCK = (By.XPATH, "//a[@title='Yellow Duck']")
