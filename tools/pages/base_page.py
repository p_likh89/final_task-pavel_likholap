from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class BasePage:

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        """
        Method opens particular
        web page using passed url
        """
        self.driver.get(self.url)

    def find_element(self, locator, timeout=10):
        """
        Method return
        found web element
        """
        try:
            e = WebDriverWait(self.driver,
                              timeout).\
                until(ec.presence_of_element_located(locator))
            return e
        except TimeoutException:
            return None
