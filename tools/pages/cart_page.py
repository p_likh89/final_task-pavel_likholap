import time
from tools.pages.base_page import BasePage
from tools.locators.cart_page_locators import CartPageLocators


class CartPage(BasePage):

    def get_final_price(self):
        """
        :return: Finally price of items
        from cart
        """
        pcs_field = self.find_element(CartPageLocators.CART_AMOUNT)
        pcs_field.clear()
        pcs_field.send_keys(3)
        self.find_element(CartPageLocators.CART_UPDATE).click()
        time.sleep(5)
        return self.find_element(
            CartPageLocators.CART_FINALLY_PRICE)

    def remove_items_from_cart(self):
        """
        :return: Web element - text of
        blank cart
        """
        self.get_final_price()
        self.find_element(
            CartPageLocators.CART_REMOVE_BTN).click()
        return self.find_element(CartPageLocators.CART_BLANK_TEXT)
