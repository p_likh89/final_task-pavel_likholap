import time
from tools.pages.base_page import BasePage
from tools.locators.duck_page_locators import DuckPageLocators
from tools.pages.cart_page import CartPage


class DuckPage(BasePage):

    def add_items_to_cart(self):
        """
        :return: CartPage object
        (cart with added items)
        """
        self.driver.implicitly_wait(10)
        self.find_element(DuckPageLocators.DUCK_DD1).click()
        self.find_element(DuckPageLocators.DUCK_DD2).click()
        self.find_element(DuckPageLocators.DUCK_CART_BTN).click()
        time.sleep(5)
        self.find_element(DuckPageLocators.DUCK_CART).click()
        return CartPage(self.driver, self.driver.current_url)
