from tools.pages.base_page import BasePage
from tools.locators.edit_page_locators import EditPageLocators
import mysql.connector as mysql


class EditPage(BasePage):

    def change_user_name(self):
        """
        :return: Message about successful
        profile editing
        """
        name_field = self.find_element(EditPageLocators.EDIT_NAME)
        name_field.clear()
        name_field.send_keys('Renamed')
        self.find_element(EditPageLocators.EDIT_SAVE).click()
        return self.find_element(EditPageLocators.EDIT_NOTICE)

    @staticmethod
    def get_name_from_database():
        """
        :return: Item from database (customer firstname)
        """
        db = mysql.connect(
            host="192.168.99.1",
            user="admin",
            passwd="qwe123QWE",
            database="litecart"
        )
        cursor = db.cursor()
        query = "SELECT firstname from lc_customers where id=2"
        cursor.execute(query)
        return cursor.fetchall()
