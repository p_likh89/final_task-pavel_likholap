from tools.pages.base_page import BasePage
from tools.pages.duck_page import DuckPage
from tools.pages.edit_page import EditPage
from tools.locators.main_page_locators import MainPageLocators


class MainPage(BasePage):
    URL = 'http://192.168.99.1/litecart/en/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def perform_login(self):
        """
        :return: Web element which appeared after successful
        login
        """
        email = self.find_element(MainPageLocators.MAIN_EMAIL)
        email.send_keys('7869727@bk.ru')
        psswd = self.find_element(MainPageLocators.MAIN_PASSWORD)
        psswd.send_keys('qwe123QWE')
        self.find_element(MainPageLocators.MAIN_LOGIN_BTN).click()
        return self.find_element(MainPageLocators.MAIN_ACCOUNT)

    def open_edit_page(self):
        """
        :return: EditPage object
        """
        self.find_element(MainPageLocators.MAIN_EDIT).click()
        return EditPage(self.driver, self.driver.current_url)

    def get_duck_page(self):
        """
        :return: DuckPage object
        """
        self.find_element(MainPageLocators.MAIN_DUCK).click()
        return DuckPage(self.driver, self.driver.current_url)
