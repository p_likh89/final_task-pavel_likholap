import time

from requests import get, post, put, delete


class RestApi:
    URL = 'https://petstore.swagger.io/v2/user/'

    def add_new_user(self, user: dict):
        """
        :param user: Json user data
        :return: Value of response code
        """
        r = post(self.URL, json=user)
        return r.status_code

    def get_user_name(self, user_name: str):
        """
        :param user_name: Username
        :return: List which contains user json data
        and value of response code
        """
        r = get(f"{self.URL}{user_name}")
        while r.status_code == 404:
            time.sleep(30)
            r = get(f"{self.URL}{user_name}")
        else:
            return r.text, r.status_code

    def change_user_name(self, user_name: str, new: dict):
        """
        :param user_name: Exist username
        :param new: Username to change
        :return: Value of response code
        """
        r = put(f"{self.URL}{user_name}", json=new)
        return r.status_code

    def remove_user(self, user_name: str):
        """
        :param user_name: Exist username
        :return: Value of response code
        """
        r_delete = delete(f"{self.URL}{user_name}")
        while r_delete.status_code == 404:
            time.sleep(30)
            r_delete = get(f"{self.URL}{user_name}")
        r_get = get(f"{self.URL}{user_name}")
        while r_get.status_code == 200:
            time.sleep(30)
            r_get = get(f"{self.URL}{user_name}")
        return r_delete.status_code, r_get.status_code
